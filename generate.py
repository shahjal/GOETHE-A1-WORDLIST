import codecs
import json
import time
import genanki
import base64
import requests
import os
from hashlib import sha1
from googletrans import Translator

WLIST_PATH = 'GOETHE-ZERTIFIKAT A1 START DEUTSCH 1 WORDLIST.txt'
GTTS_API_KEY = 'AIzaSyBm7NinfOuybtq-WWkcFMoiHHxMz60qxj0'

MODEL_ID = 1837104183
DECK_ID = 1995435403

ANKI_CARD_CSS = """.card {
 font-family: arial;
 font-size: 20px;
 text-align: center;
 color: black;
 background-color: white;
}
"""

ANKI_MODEL = genanki.Model(
    MODEL_ID,
    'Simple Model',
    fields=[
        {'name': 'Front'},
        {'name': 'Back'},
    ],
    templates=[
        {
            'name': 'Card 1',
            'qfmt': '{{Front}}',
            'afmt': '{{FrontSide}}<hr id=answer>{{Back}}',
        },
    ],
    css=ANKI_CARD_CSS)

ANKI_DECK = genanki.Deck(DECK_ID, 'A1 WORDLIST')
ANKI_PACKAGE = genanki.Package(ANKI_DECK)
ANKI_PACKAGE.media_files = list()
TRANS = Translator()

def text_to_speech(text):
    file_name = 'sounds\\' + sha1(text.encode("utf-8")).hexdigest() + '.mp3'
    if os.path.exists(file_name):
        return file_name, True

    payload = {
        "audioConfig": {
            "audioEncoding": "MP3",
            "pitch": 0.00,
            "speakingRate": 0.85,
        },
        "input": {
            "ssml": "<speak>%s</speak>" % text
        },
        "voice": {
            "languageCode": 'de-DE',
            "name": 'de-DE-Wavenet-A',
        }
    }

    headers = {}
    if sha1(GTTS_API_KEY.encode("utf-8")).hexdigest() == "8224a632410a845cbb4b20f9aef131b495f7ad7f":
        headers['x-origin'] = 'https://explorer.apis.google.com'

    r = requests.post("https://texttospeech.googleapis.com/v1/text:synthesize?key={}".format(GTTS_API_KEY), 
        headers=headers, 
        json=payload
        )

    r.raise_for_status()
    data = r.json()
    encoded = data['audioContent']
    audio_content = base64.b64decode(encoded)

    with open(file_name, 'wb') as response_output:
        response_output.write(audio_content)

    return file_name, False

def add_card(front, back, sounds= []):
    note = genanki.Note(ANKI_MODEL, fields=[front, back])
    ANKI_DECK.add_note(note)
    if sounds != []:
        ANKI_PACKAGE.media_files.extend(sounds)

def dump_deck(name):
    ANKI_PACKAGE.write_to_file(name + '.apkg')

def append_translation(word):
    wt = TRANS.translate(word['word'], src='de').text
    st = [TRANS.translate(s, src='de').text for s in word['sentence']]
    word.update({'word_translation' : wt, 'sentence_translation': st })

with codecs.open(WLIST_PATH, encoding='utf-8') as f:
    content = f.read()
    content = content.split('\r\n\r\n')
    
wlist = list()
for c in content:
    c = c.split('\r\n')
    wlist.append({
        'word': c[0].strip(), 
        'sentence': [c[i].strip() for i in range(1, len(c))]
        })

with open('GOETHE-ZERTIFIKAT A1 START DEUTSCH 1 WORDLIST.json', 'w', encoding='utf8') as jf:
    json.dump(wlist, jf, ensure_ascii= False, indent= 4)

request_count = 0
for count, word in enumerate(wlist):
    speech = list()
    for s in word['sentence']:
        sound, existed = text_to_speech(s)
        speech.append(sound)

        if not existed:
            request_count += 1

        if request_count >= 50:
            print("About 50 request sent, waiting for 1 min...")
            time.sleep(65)
            request_count = 0

    word.update({'sentence_speech' : speech})
    append_translation(word)

    if count % 10 == 0:
        print("Progress (%d/%d)..." % (count, len(wlist)))

with open('GOETHE-ZERTIFIKAT A1 START DEUTSCH 1 WORDLIST TRANSLATED.json', 'w', encoding='utf8') as jf:
    json.dump(wlist, jf, ensure_ascii= False, indent= 4)

for word in wlist:
    sentence = ''
    for i, s in enumerate(word['sentence']):
        sentence += '%i- %s<br>[sound:%s]<br>' % (i, s, word['sentence_speech'][i])

    sentence_translation = ''
    for i, s in enumerate(word['sentence_translation']):
        sentence_translation += '%i- %s<br>' % (i, s)

    front = word['word']
    back = "<br>SOUND<br><br>" + sentence + "<br><br><br>" + sentence_translation + "<br>PICTURE<br>"
    
    add_card(front, back, word['sentence_speech'])

dump_deck('A1 WORDLIST')



    






    


